using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimerCountdown : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject textDisplay;
    public int secondsLeft = 59;
    public bool takingAway = false;
    public GameEnding gameEnding;

    void Start()
    {
        textDisplay.GetComponent<Text>().text = "00:" + secondsLeft;
    }
    void Update()
    {
        if (takingAway == false && secondsLeft > 0)
        {
            StartCoroutine(TimerTake());
        }
    }
    IEnumerator TimerTake()
    {
        takingAway = true;
        yield return new WaitForSeconds(1);
        secondsLeft -= 1;
        if (secondsLeft < 10)
        {
            textDisplay.GetComponent<Text>().text = "00:0" + secondsLeft;
        } else {
            textDisplay.GetComponent<Text>().text = "00:" + secondsLeft;
        }
        if (secondsLeft == 0)
        {
            Die();
        }
        takingAway = false;
    }
    void Die()
    {
        gameEnding.CaughtPlayer ();
    }
    

}
