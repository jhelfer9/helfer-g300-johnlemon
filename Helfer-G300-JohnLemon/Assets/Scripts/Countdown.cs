using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    float currentTime = 0f;
    float startingTime = 10f;
    public GameObject countTextObject;
    public GameObject uTextObject;
    public GameObject cTextObject;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = startingTime;
        countTextObject.SetActive(true);
        uTextObject.SetActive(false);
        cTextObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        if (currentTime <= 0)
        {
            currentTime = 0;
            countTextObject.SetActive(false);
            uTextObject.SetActive(true);
            cTextObject.SetActive(true);
        }
    }
}
